package bigdata.java.platform.service.impl;

import bigdata.java.platform.beans.SparkJob;
import bigdata.java.platform.beans.SysSet;
import bigdata.java.platform.beans.TTask;
import bigdata.java.platform.beans.UserInfo;
import bigdata.java.platform.service.SystemService;
import bigdata.java.platform.service.TaskService;
import bigdata.java.platform.service.UserService;
import bigdata.java.platform.util.Comm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    SystemService systemService;
    @Autowired
    TaskService taskService;

    @Override
    public UserInfo doLogin(String userName, String passWord) {
        String sql = "select u.*,s.queueName from UserInfo u left join SysSet s on u.userId = s.userId where username=:username and password=:password";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("username",userName);
        parameters.addValue("password",passWord);
        UserInfo userInfo = namedParameterJdbcTemplate.queryForObject(sql, parameters, UserInfo.class);
        return userInfo;
    }

    @Override
    public Boolean modifyPwd(String userName, String newPassWord) {
        String sql ="update UserInfo set password=:password where username=:username";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("username",userName);
        parameters.addValue("password",newPassWord);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        return update > 0;
    }

    @Transactional
    @Override
    public Boolean add(UserInfo userInfo) {
        String sql = "insert into UserInfo (NickName,UserName,PassWord,IsAdmin,warnPhone)values(:NickName,:UserName,:PassWord,:IsAdmin,:warnPhone)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource parameters = new BeanPropertySqlParameterSource(userInfo);
        int update = namedParameterJdbcTemplate.update(sql, parameters,keyHolder,new String[]{"UserId"});
        userInfo.setUserId(keyHolder.getKey().intValue());
        SysSet sysSet = new SysSet();
        sysSet.setUserId(userInfo.getUserId());
        sysSet.setQueueName(userInfo.getQueueName());
        systemService.add(sysSet);
        Comm.userQuaueNameMap.put(userInfo.getUserId(),userInfo.getQueueName());
        return update > 0;
    }

    @Transactional
    @Override
    public Boolean edit(UserInfo userInfo) {
        String sql = "update UserInfo set  NickName=:NickName,IsAdmin=:IsAdmin,PassWord=:PassWord,warnPhone=:warnPhone where UserId=:UserId";
        SqlParameterSource parameters = new BeanPropertySqlParameterSource(userInfo);
        //更新用户信息
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        SysSet sysSet = systemService.get(userInfo.getUserId());
        sysSet.setQueueName( userInfo.getQueueName());
        //更新系统设置的queueName
        systemService.edit(sysSet);
        //更新所有任务的queueName
        taskService.updateQueueName(userInfo.getUserId(),userInfo.getQueueName());
        //更新queueName缓存信息
        Comm.userQuaueNameMap.put(userInfo.getUserId(),userInfo.getQueueName());
        return update > 0;
    }

    @Override
    public Boolean delete(Integer userId) {
        String sql = "delete from UserInfo  where UserId=:UserId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("UserId",userId);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        return update > 0;
    }

    @Override
    public UserInfo get(Integer userId) {
        String sql = "select u.*,s.queueName from UserInfo u left join SysSet s on u.userId = s.userId where u.UserId=:UserId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("UserId",userId);
        UserInfo userInfo = namedParameterJdbcTemplate.queryForObject(sql, parameters, UserInfo.class);
        return userInfo;
    }

    @Override
    public List<UserInfo> list() {
        String sql = "select u.*,s.queueName from UserInfo u left join SysSet s on u.userId = s.userId";
        List<UserInfo> list = namedParameterJdbcTemplate.getJdbcTemplate().queryForList(sql, UserInfo.class);
        return list;
    }
}

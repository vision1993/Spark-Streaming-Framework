package bigdata.java.platform.service.impl;

import bigdata.java.platform.beans.TTask;
import bigdata.java.platform.beans.TaskLog;
import bigdata.java.platform.beans.livy.StartResoult;
import bigdata.java.platform.service.TaskLogService;
import bigdata.java.platform.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class TaskLogServiceImpl implements TaskLogService {

    @Autowired
    TaskService taskService;

    @Autowired
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public TaskLog get(Integer taskLogId) {
        return null;
    }

    @Override
    public void add(TaskLog taskLog) {

    }

    @Override
    public void edit(TaskLog taskLog) {

    }

    @Override
    public Boolean updateMonitor(Integer taskLogId, Integer waitingBatches, Integer totalCompletedBatches, Long totalProcessedRecords) {
        String sql = "update TaskLog  set waitingBatches=:waitingBatches,totalCompletedBatches=:totalCompletedBatches,totalProcessedRecords=:totalProcessedRecords,updateTime=:updateTime where taskLogId=:taskLogId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("waitingBatches",waitingBatches);
        parameters.addValue("totalCompletedBatches",totalCompletedBatches);
        parameters.addValue("totalProcessedRecords",totalProcessedRecords);
        parameters.addValue("taskLogId",taskLogId);
        parameters.addValue("updateTime",new Date());
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        return update > 0;
    }

    @Override
    public TaskLog addTaskId(Integer taskId, Date updateTime) {
        String sql = "insert into TaskLog (taskId,updateTime)values(:taskId,:updateTime)";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("taskId",taskId);
        parameters.addValue("updateTime",updateTime);
//        int update = namedParameterJdbcTemplate.update(sql, parameters);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        int update = namedParameterJdbcTemplate.update(sql, parameters,keyHolder,new String[]{"taskLogId"});
        TaskLog taskLog = new TaskLog();
        taskLog.setTaskId(taskId);
        taskLog.setTaskLogId(keyHolder.getKey().intValue());
        taskLog.setUpdateTime(updateTime);
        if(update > 0)
        {
            return taskLog;
        }
        else
        {
            return null;
        }
    }

    @Override
    public Boolean deleteByTaskId(Integer taskId) {
        String sql = "delete from TaskLog  where taskId=:taskId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("taskId",taskId);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        return update > 0;
    }

    @Override
    public List<TaskLog> listForGroupMaxId()
    {
        String sql ="select * from TaskLog t where taskLogId = (select max(taskLogId) from TaskLog where taskId = t.taskId order by taskLogId)";
        List<TaskLog> query = namedParameterJdbcTemplate.query(sql, new BeanPropertyRowMapper<>(TaskLog.class));
        return query;
    }

    @Override
    public List<TaskLog> list(Integer taskId) {
        String sql ="select * from TaskLog where taskId =:taskId order by tasklogid desc limit 10";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("taskId",taskId);
        List<TaskLog> query = namedParameterJdbcTemplate.query(sql, parameters,new BeanPropertyRowMapper<>(TaskLog.class));
        return query;
    }

    @Transactional
    @Override
    public Boolean addBatchId(Integer taskId, Integer batchId, Date updateTime) {
        String sql = "insert into TaskLog (taskId,batchId,updateTime)values(:taskId,:batchId,:updateTime)";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("taskId",taskId);
        parameters.addValue("batchId",batchId);
        parameters.addValue("updateTime",updateTime);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        if(update > 0)
        {
            taskService.setStatus(taskId,TTask.LIVYSTARTING,updateTime);
            taskService.updateStartTime(taskId,updateTime);
        }
        return update > 0;
    }

    @Transactional
    @Override
    public Boolean updateBatchId(Integer taskId,Integer taskLogId, Integer batchId) {
        Date date = new Date();
        String sql = "update TaskLog set batchId=:batchId, updateTime=:updateTime where taskLogId=:taskLogId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("taskLogId",taskLogId);
        parameters.addValue("batchId",batchId);
        parameters.addValue("updateTime",date);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        if(update > 0)
        {
            taskService.setStatus(taskId,TTask.LIVYSTARTING,date);
            taskService.updateStartTime(taskId,date);
        }
        return update > 0;
    }
    @Transactional
    @Override
    public Boolean stop(Integer taskId, Integer taskLogId, Integer batchId) {

        Date date = new Date();
        String sql = "update TaskLog set updateTime=:updateTime where taskLogId=:taskLogId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("taskLogId",taskLogId);
        parameters.addValue("updateTime",date);
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        if(update > 0)
        {
            taskService.setStatus(taskId,TTask.STOPED,date);
        }
        return update > 0;
    }

    @Transactional
    @Override
    public Boolean updateAppId(Integer taskId, Integer taskLogId, Integer batchId, StartResoult startResoult) {
        Date date = new Date();
        String sql = "update TaskLog set applicationid=:applicationid,driver=:driver,sparkUI=:sparkUI,updateTime=:updateTime  where taskId=:taskId and taskLogId=:taskLogId and batchId=:batchId";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("taskId",taskId);
        parameters.addValue("taskLogId",taskLogId);
        parameters.addValue("batchId",batchId);
        parameters.addValue("updateTime",date);
        parameters.addValue("applicationid",startResoult.getAppId());
        parameters.addValue("driver",startResoult.getAppInfo().getDriverLogUrl());
        parameters.addValue("sparkUI",startResoult.getAppInfo().getSparkUiUrl());
        int update = namedParameterJdbcTemplate.update(sql, parameters);
        if(update > 0)
        {
            taskService.setStatus(taskId,TTask.STARTED,date);
//            taskService.updateErrCountAndTryCount(taskId,0,0,date);
        }
        return update > 0;
    }
}

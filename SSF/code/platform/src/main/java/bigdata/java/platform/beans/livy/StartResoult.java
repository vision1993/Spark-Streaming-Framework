package bigdata.java.platform.beans.livy;

public class StartResoult {
    Integer id;
    String state;
    String appId;
    AppInfo appInfo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public AppInfo getAppInfo() {
        return appInfo;
    }

    public void setAppInfo(AppInfo appInfo) {
        this.appInfo = appInfo;
    }

    @Override
    public String toString() {
        return "StartResoult{" +
                "id=" + id +
                ", state='" + state + '\'' +
                ", appId='" + appId + '\'' +
                ", appInfo=" + appInfo +
                '}';
    }
}

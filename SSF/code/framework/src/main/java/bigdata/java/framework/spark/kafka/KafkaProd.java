/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.kafka;

import bigdata.java.framework.spark.util.ConfigUtil;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.List;
import java.util.Map;
import java.util.Properties;
/**
 *kafka生产者工具类(已废弃，建议使用bigdata.java.framework.spark.util.client.KafkaProducerClient)
 */
@Deprecated
public class KafkaProd {
    private static KafkaProducer<String, String> producer;

    static {
        String kafkaBroker = ConfigUtil.getConfig().getString("kafkaparam.bootstrap.servers");
        Properties properties = new Properties();
        properties.put("bootstrap.servers", kafkaBroker);// kafka地址
        properties.put("acks", "-1");
        properties.put("value.serializer", StringSerializer.class);
        properties.put("key.serializer", StringSerializer.class);
        producer = new KafkaProducer<String, String>((properties));
    }
    public static KafkaProducer<String, String> createProducer() {
        return producer;
    }

    public static void sendMessageNoClose(String topicName, String strMessage) {
        KafkaProducer<String, String> producer = createProducer();
        producer.send(new ProducerRecord<String, String>(topicName, strMessage));
    }

    public static void sendMessage(String topicName, String strMessage) {
        KafkaProducer<String, String> producer = createProducer();
        producer.send(new ProducerRecord<String, String>(topicName, strMessage));
        producer.close();
    }

    public static void sendMessageNoClose(String topicName, String... strMessages) {
        KafkaProducer<String, String> producer = createProducer();
        for (String strMessage : strMessages) {
            producer.send(new ProducerRecord<String, String>(topicName, strMessage));
        }
    }

    public static void sendMessage(String topicName, String... strMessages) {
        KafkaProducer<String, String> producer = createProducer();
        for (String strMessage : strMessages) {
            producer.send(new ProducerRecord<String, String>(topicName, strMessage));
        }
        producer.close();
    }

    public static void sendMessage(String topicName, List<String[]> strMessages, Boolean sendKey) {
        KafkaProducer<String, String> producer = createProducer();
        for (String[] strMessage : strMessages) {
            if(sendKey)
            {
                producer.send(new ProducerRecord<String, String>(topicName, strMessage[0],strMessage[1]));
            }
            else
            {
                producer.send(new ProducerRecord<String, String>(topicName, strMessage[1]));
            }
        }
//        producer.close();
    }

    public static void sendMessage(String topicName, List<Map<Object, Object>> mapMessages) {
        KafkaProducer<String, String> producer = createProducer();
        for (Map<Object, Object> mapMessage : mapMessages) {
            String array = mapMessage.toString();
            producer.send(new ProducerRecord<String, String>(topicName, array));
        }
        producer.close();
    }

    public static void sendMessage(String topicName, Map<Object, Object> mapMessage) {
        KafkaProducer<String, String> producer = createProducer();
        String array = mapMessage.toString();
        producer.send(new ProducerRecord<String, String>(topicName, array));
        producer.close();
    }

}

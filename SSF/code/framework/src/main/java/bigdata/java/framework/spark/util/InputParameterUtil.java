/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.util;

import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.Durations;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * java args参数校验类
 */
public class InputParameterUtil implements Serializable {

    /**
     * 参数校验构造函数
     * @param args 参数
     * @param length 参数长度
     */
    public InputParameterUtil(String[] args,int length)
    {
        this(args,length,ARGSCOUNTERROR);
    }

    /**
     * 获取参数内容
     */
    public String[] getArgs() {
        return args;
    }

    /**
     * 设置参数内容
     */
    public void setArgs(String[] args) {
        this.args = args;
    }
    //参数
    private String[] args;

    /**
     * 参数校验构造函数
     * @param args 参数
     * @param length 参数长度
     * @param errorMsg 长度不符合，需要抛出的异常信息
     */
    public InputParameterUtil(String[] args,int length,String errorMsg)
    {
        if(length !=0)
        {
            checkLength(args,length,errorMsg);
        }
        setArgs(args);
    }

    /**
     * 应用程序名称
     */
    public static final String APPNAME = "appname";
    /**
     * topics名称，多个用逗号隔开
     */
    public static final String TOPICS = "topics";
    /**
     * 消费者组
     */
    public static final String GROUPID = "groupid";
    /**
     * 时间间隔
     */
    public static final String DURATION = "duration";
    /**
     * 最大条数
     */
    public static final String MAXNUM = "maxnum";

    /**
     * 检查点目录
     */
    public static final String CHECKPOINT = "checkpoint";
    /**
     * 长度默认错误信息
     */
    public static final  String ARGSCOUNTERROR = "args parameter count error";

    /**
     * 参数长度验证
     * @param args args参数
     * @param length 长度
     * @param errorMsg 错误信息
     */
    public static void checkLength(String[] args,int length,String errorMsg){
        if(args == null || args.length != length){
            throw new  RuntimeException("args:"+Arrays.toString(args) +",Error:"+ errorMsg);
        }
    }
    /**
     * 获取应用程序名称
     */
    public String getAppName()
    {
        return getValue(APPNAME,getArgs());
    }
    /**
     * 获取主题名称，多个用逗号隔开
     */
    public String getTopics()
    {
        return getValue(TOPICS,getArgs());
    }
    /**
     * 获取消费者组
     */
    public String getGroupId()
    {
        return getValue(GROUPID,getArgs());
    }

    /**
     * 获取时间间隔
     */
    public Long getDuration()
    {
        String value = getValue(DURATION, getArgs());
        return Long.valueOf(value);
    }

    /**
     * 获取时间间隔（秒）
     */
    public Duration getSeconds()
    {
        return Durations.seconds(getDuration());
    }

    /**
     * 获取最大条数
     */
    public Integer getMaxNum()
    {
        String value = getValue(MAXNUM, getArgs());
        return Integer.valueOf(value);
    }

    /**
     * 获取是否设置检查点目录
     */
    public Boolean getIsCheckPoint()
    {
        String value = getValue(CHECKPOINT);
        if(value.equals("true"))
        {
            return true;
        }
        return false;
    }

    /**
     * 根据key获取value值
     * @param key 查找的key
     */
    public String getValue(String key)
    {
        return getValue(key,getArgs());
    }

    /**获取所有的key
     * @param args 参数
     */
    public static String[] getAllKey(String[] args)
    {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            String[] split = arg.split(":");
            if(split.length!=2)
            {
                throw new  RuntimeException("main args parameters format error");
            }
            list.add(split[0]);
        }
        if(list.size() > 0)
        {
            return list.toArray(new String[list.size()]);
        }
        else
        {
            return new String[]{};
        }
    }

    /**根据前缀获获取所有配置项
     * @param prev  前缀
     * @param args 参数
     */
    public static String[] getPrevList(String prev,String[] args)
    {
        if(prev==null || prev =="")
        {
            throw new  RuntimeException("key is null or empty");
        }

        if(args == null || args.length == 0) {
            throw new  RuntimeException("main args parameters is null or empty");
        }
        List<String > list = new ArrayList<>();

        for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            String[] split = arg.split(":");
            if(split.length!=2)
            {
                throw new  RuntimeException("main args parameters format error");
            }
            if(split[0].contains(prev))
            {
                list.add(arg);
            }
        }
        if(list.size() > 0)
        {
            return  list.toArray(new String[list.size()]);
        }
        else
        {
            return new String[]{};
        }
    }

    /**
     * 通过key 查找对于的value
     * @param key 查找的key
     * @param args args参数
     * @return 没有找到返回空字符
     */
    public static String getValue(String key,String[] args)
    {
        if(key==null || key =="")
        {
            throw new  RuntimeException("key is null or empty");
        }

        if(args == null || args.length == 0) {
            throw new  RuntimeException("main args parameters is null or empty");
        }

        for (int i = 0; i < args.length; i++) {
            String arg = args[i];
            String[] split = arg.split(":");
            if(split.length!=2)
            {
                throw new  RuntimeException("main args parameters format error");
            }
            if(key.equals(split[0]))
            {
                return split[1];
            }
        }
        return "";
    }
}

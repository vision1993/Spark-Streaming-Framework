/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.util.client;

import bigdata.java.framework.spark.pool.elasticsearch5.ESConfig;
import bigdata.java.framework.spark.pool.elasticsearch5.ESConnectionPool;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.admin.cluster.state.ClusterStateResponse;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequest;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequestBuilder;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.cluster.node.DiscoveryNode;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryAction;
import org.elasticsearch.search.SearchHit;

import java.util.*;
import java.util.stream.Collectors;

/**
 * ElasticSearch5客户端工具类
 */
public class ESClient {
    private static ESClient instance=null;
    public static ESClient getInstance(){
        if(instance == null){
            synchronized(ESClient.class){
                if(instance==null){
                    instance = new ESClient();
                }
            }
        }
        return instance;
    }
    ESConnectionPool esConnectionPool;
    private ESClient()
    {
        System.setProperty("es.set.netty.runtime.available.processors","false");
        ESConfig esConfig = new ESConfig();
//        esConfig.setMaxTotal(2);
        esConnectionPool = new ESConnectionPool(esConfig);
    }

    public ESConnectionPool getConnectionPool() {
        return esConnectionPool;
    }

    /**
     * 归还链接
     * @param pool 连接池对象
     * @param resource 连接对象
     */
    public static void returnConnection(ESConnectionPool pool,TransportClient resource){
        if(pool!=null && resource!=null)
        {
            pool.returnConnection(resource);
        }
    }

    /**
     * 查看集群信息
     */
    public void getClusterInfo() {

        ESConnectionPool pool=null;
        TransportClient resource=null;
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            List<DiscoveryNode> nodes = resource.connectedNodes();
            for (DiscoveryNode node : nodes) {
                System.out.println("HostId:"+node.getHostAddress()+" hostName:"+node.getHostName()+" Address:"+node.getAddress());
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
    }

    /**
     * 验证索引是否存在
     * @param index 索引名
     */
    public boolean indexExist(String index) {
        ESConnectionPool pool=null;
        TransportClient resource=null;
        IndicesExistsResponse inExistsResponse=null;
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            IndicesExistsRequest inExistsRequest = new IndicesExistsRequest(index);
            inExistsResponse = resource.admin().indices()
                    .exists(inExistsRequest).actionGet();
        }
        catch (Exception e)
        {
            throw new  RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
        return inExistsResponse.isExists();
    }

    /**
     * 插入数据
     * @param index 索引名
     * @param type 类型
     * @param json json数据
     */
    public Long insertData(String index, String type, String json) {
        return insertData(index,type,null,json);
    }

    /**
     * 插入文档，参数为json字符串
     * @param index 索引名
     * @param type 类型
     * @param _id 数据id
     * @param json 数据
     */
    public Long insertData(String index, String type, String _id, String json) {
        ESConnectionPool pool=null;
        TransportClient resource=null;
        IndexResponse response = null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            if(StringUtils.isBlank(_id))
            {
                response = resource.prepareIndex(index, type)
                        .setSource(json)
                        .get();
            }
            else
            {
                response = resource.prepareIndex(index, type).setId(_id)
                        .setSource(json)
                        .get();
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
        return response.getVersion();
    }

    /**
     * 更新数据
     * @param index 索引名
     * @param type  类型
     * @param _id   数据id
     * @param json  数据
     */
    public void updateData(String index, String type, String _id, String json){
        ESConnectionPool pool=null;
        TransportClient resource=null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            UpdateRequest updateRequest = new UpdateRequest(index, type, _id).doc(json);
            resource.update(updateRequest).get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
    }

    /**
     * 删除指定文档数据
     * @param index 索引名
     * @param type 类型
     * @param _id 数据id
     */
    public void deleteData(String index, String type, String _id) {
        ESConnectionPool pool=null;
        TransportClient resource=null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            DeleteResponse response = resource.prepareDelete(index, type, _id).get();
//            System.out.println(response.isFragment());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
    }

    /**
     * 根据查询进行删除(同步)
     * @param index 索引名
     * @param filter 过滤条件
     */
    public void delete_by_query_sync(String index, QueryBuilder filter)
    {
        ESConnectionPool pool=null;
        TransportClient resource=null;
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            BulkByScrollResponse bulkByScrollResponse = DeleteByQueryAction.INSTANCE.newRequestBuilder(resource)
                    .filter(filter)
                    .source(index).get();
            long deleted = bulkByScrollResponse.getDeleted();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
    }
    /**
     * 根据查询进行删除(异步)
     * @param index 索引名
     * @param filter 过滤条件
     */
    public void delete_by_query_asyn(String index, QueryBuilder filter)
    {
        ESConnectionPool pool=null;
        TransportClient resource=null;
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            DeleteByQueryAction.INSTANCE.newRequestBuilder(resource)
                    .filter(filter)
                    .source(index).execute(new ActionListener<BulkByScrollResponse>() {
                @Override
                public void onResponse(BulkByScrollResponse bulkByScrollResponse) {
                    long deleted = bulkByScrollResponse.getDeleted();
                }

                @Override
                public void onFailure(Exception e) {
                    throw new RuntimeException(e);
                }
            });
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
    }

    /**
     * 根据id进行批量删除文档
     * @param index 索引名
     * @param type 类型
     * @param _ids ids
     */
    public void deleteBatchData(String index, String type, List<String> _ids)
    {
        ESConnectionPool pool=null;
        TransportClient resource=null;
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            BulkRequestBuilder builder = resource.prepareBulk();
            for (int i = 0; i < _ids.size(); i++) {
                String _id = _ids.get(i);
                builder.add(resource.prepareDelete(index,type,_id));
            }
            BulkResponse bulkItemResponses = builder.get();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
    }

    /**
     * 批量删除索引库
     * @param list 索引库list
     */
    public void deleteIndex(List<String> list)
    {
        if(list==null)return;
        for (int i = 0; i < list.size(); i++) {
            deleteIndex(list.get(i));
        }
    }

    /**
     * 删除索引库
     * @param index 索引名
     */
    public void deleteIndex(String index) {
        if(StringUtils.isBlank(index))
        {
            return;
        }
        ESConnectionPool pool=null;
        TransportClient resource=null;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            if (indexExist(index)) {
                DeleteIndexResponse dResponse = resource.admin().indices().prepareDelete(index)
                        .execute().actionGet();
                if (!dResponse.isAcknowledged()) {
                    throw new RuntimeException("failed to delete index " + index + "!");
                }else {
                    System.out.println("delete index " + index + " successfully!");
                }
            } else {
                throw new RuntimeException("delete index " + index + " not exists!");
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
    }

    /**
     * 删除索引类型表所有数据，批量删除
     * @param index 索引名
     * @param type 类型
     */
    public void deleteIndexTypeAllData(String index, String type) {
        ESConnectionPool pool=null;
        TransportClient resource=null;
        try{
            pool = getConnectionPool();
            resource = pool.getConnection();
            SearchResponse response = resource.prepareSearch(index).setTypes(type)
                    .setQuery(QueryBuilders.matchAllQuery()).setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                    .setScroll(new TimeValue(60000)).setSize(10000).setExplain(false).execute().actionGet();
            BulkRequestBuilder bulkRequest = resource.prepareBulk();
            while (true)
            {
                SearchHit[] hitArray = response.getHits().getHits();
                SearchHit hit = null;
                for (int i = 0, len = hitArray.length; i < len; i++) {
                    hit = hitArray[i];
                    DeleteRequestBuilder request = resource.prepareDelete(index, type, hit.getId());
                    bulkRequest.add(request);
                }
                BulkResponse bulkResponse = bulkRequest.execute().actionGet();
                if (bulkResponse.hasFailures()) {
                    throw new RuntimeException(bulkResponse.buildFailureMessage());
                }
                if (hitArray.length == 0) break;
                response = resource.prepareSearchScroll(response.getScrollId())
                        .setScroll(new TimeValue(60000)).execute().actionGet();
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
    }

    /**
     * 批量插入文档数据
     * @param index 索引名
     * @param type 类型
     * @param jsonList 批量数据json格式
     */
    public void bulkInsertData(String index, String type, List<String> jsonList) {
        ESConnectionPool pool=null;
        TransportClient resource=null;
        BulkResponse bulkResponse =null;
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            BulkRequestBuilder bulkRequest = resource.prepareBulk();

            for (int i = 0; i < jsonList.size(); i++) {
                String item = jsonList.get(i);
                bulkRequest.add(resource.prepareIndex(index, type).setSource(item));
            }
//            jsonList.forEach(item -> {
//                bulkRequest.add(resource.prepareIndex(index, type).setSource(item));
//            });
            bulkResponse = bulkRequest.get();
            if(bulkResponse.hasFailures()) {
                throw new RuntimeException(bulkResponse.buildFailureMessage());
            }

        }
        catch ( Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
    }

    /**
     * 批量插入文档数据
     * @param index 索引名
     * @param type 类型
     * @param jsonList 批量数据json格式
     */
    public void bulkInsertDataForId(String index, String type, List<String[]> jsonList) {
        ESConnectionPool pool=null;
        TransportClient resource=null;
        BulkResponse bulkResponse =null;
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            BulkRequestBuilder bulkRequest = resource.prepareBulk();
//            jsonList.forEach(item -> {
//                String _id = item[0];
//                String json = item[1];
//                bulkRequest.add(resource.prepareIndex(index, type,_id).setSource(json));
//            });
            for (int i = 0; i < jsonList.size(); i++) {
                String[] item = jsonList.get(i);
                String _id = item[0];
                String json = item[1];
                bulkRequest.add(resource.prepareIndex(index, type,_id).setSource(json));
            }

            bulkResponse = bulkRequest.get();
            if(bulkResponse.hasFailures()) {
                throw new RuntimeException(bulkResponse.buildFailureMessage());
            }
        }
        catch ( Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
    }

    /**
     * 根据id获取指定文档
     * @param index 索引名
     * @param type 类型
     * @param id id
     * @return 返回json
     */
    public String getDocumentByIdAsString(String index, String type, String id) {
        ESConnectionPool pool=null;
        TransportClient resource=null;
        String json ="";
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            // 搜索数据
            GetResponse response = resource.prepareGet(index, type, id)
//                .setOperationThreaded(false)    // 线程安全
                    .get();
            if(response.isExists())
            {
                json =response.getSourceAsString();
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
        return json;
    }

    /**
     * 根据多个id批量获取指定文档信息
     * @param index 索引名
     * @param type 类型
     * @param ids ids
     * @return 返回json list
     */
    public List<String> getMultiDocumentByIdAsString(String index, String type, String... ids) {
        ESConnectionPool pool=null;
        TransportClient resource=null;
        List<String> list = new ArrayList<>();
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            MultiGetResponse responses = resource.prepareMultiGet()
                    .add(index,type,ids)
                    .get();

            for (MultiGetItemResponse itemResponses : responses)
            {
                GetResponse resp = itemResponses.getResponse();
                if(resp.isExists())
                {
                    list.add(resp.getSourceAsString());
                }
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
        return list;
    }

    /**
     * 根据多个id批量获取指定文档信息
     * @param index 索引名
     * @param type 类型
     * @param ids ids
     * @return 返回map list
     */
    public List<Map<String,Object>> getMultiDocumentByIdAsMap(String index, String type, String... ids) {
        ESConnectionPool pool=null;
        TransportClient resource=null;
        List<Map<String,Object>> list = new ArrayList<>();
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            MultiGetResponse responses = resource.prepareMultiGet()
                    .add(index,type,ids)
                    .get();

            for (MultiGetItemResponse itemResponses : responses)
            {
                GetResponse resp = itemResponses.getResponse();
                if(resp.isExists())
                {
                    list.add(resp.getSourceAsMap());
                }
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
        return list;
    }

    /**
     * 根据id获取指定文档信息
     * @param index 索引名
     * @param type 类型
     * @param id id
     * @return 返回map
     */
    public Map<String,Object> getDocumentByIdAsMap(String index, String type, String id) {
        ESConnectionPool pool=null;
        TransportClient resource=null;
        Map<String,Object> map = new HashMap<>();
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            // 搜索数据
            GetResponse response = resource.prepareGet(index, type, id)
//                .setOperationThreaded(false)    // 线程安全
                    .get();
            if(response.isExists())
            {
                map =response.getSourceAsMap();
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
        return map;
    }

    /**
     * 根据索引获取前5条文档
     * @param index 索引名
     * @return map list
     */
    public List<Map<String, Object>> getDocuments(String index) {
        ESConnectionPool pool=null;
        TransportClient resource=null;
        List<Map<String, Object>> mapList = new ArrayList<>();
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            // 搜索数据
            SearchResponse response = resource.prepareSearch(index)
//    			.setTypes("type1","type2"); //设置过滤type
//    			.setTypes(SearchType.DFS_QUERY_THEN_FETCH)  精确查询
//    			.setQuery(QueryBuilders.matchQuery(term, queryString));
//    			.setFrom(0) //设置查询数据的位置,分页用
//    			.setSize(60) //设置查询结果集的最大条数
//    			.setExplain(true) //设置是否按查询匹配度排序
                    .get(); //最后就是返回搜索响应信息
            System.out.println("共匹配到:"+response.getHits().getTotalHits()+"条记录!");
            SearchHit[] hits = response.getHits().getHits();
            for (SearchHit hit : hits) {
                Map<String, Object> source = hit.getSource();
                mapList.add(source);
            }
            System.out.println(response.getTotalShards());//总条数

        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
        return mapList;
    }

    /**
     * 获取指定索引库下指定type所有文档信息
     * @param index 索引名
     * @param type 类型
     * @return map list
     */
    public List<Map<String, Object>> getDocuments(String index, String type) {
        ESConnectionPool pool=null;
        TransportClient resource=null;
        List<Map<String, Object>> mapList = new ArrayList<>();
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            SearchResponse response = resource.prepareSearch(index).setTypes(type).get();
            SearchHit[] hits = response.getHits().getHits();
            for (SearchHit hit : hits) {
                Map<String, Object> source = hit.getSource();
                mapList.add(source);
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
        return mapList;
    }

    /**
     * 根据索引前缀查找不符合noContainsIndex规则的索引名称
     * @param prefix 索引前缀
     * @param noContainsIndex 排除的索引
     * @return 返回匹配的索引名
     */
    public List<String> searchIndexNoContainsIndex(String prefix,String noContainsIndex)
    {
        String index = prefix + noContainsIndex;
        List<String> list = searchIndex(prefix);
        List<String> collect = list.stream().filter(i -> !i.toLowerCase().equals(index.toLowerCase())).collect(Collectors.toList());
        return collect;
    }

    /**
     * 查找es中索引名称
     * @param prefix 索引名称
     * @return 匹配上的索引
     */
    public List<String> searchIndex(String prefix)
    {
        List<String> allIndex = getAllIndex();
        List<String> collect = allIndex.stream().filter(i -> i.toLowerCase().startsWith(prefix.toLowerCase())).collect(Collectors.toList());
        return collect;
    }

    /**
     * 获取所有的索引库
     */
    public List<String> getAllIndex(){
        ESConnectionPool pool=null;
        TransportClient resource=null;
        List<String> indexList = new ArrayList<>();
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            ClusterStateResponse response = resource.admin().cluster().prepareState().execute().actionGet();
            //获取所有索引
            String[] indexs=response.getState().getMetaData().getConcreteAllIndices();
            if(indexs!=null && indexs.length > 0)
            {
                indexList = Arrays.asList(indexs);
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
        return indexList;
    }

    /**
     * 新建索引库
     * @param indexName 索引名
     */
    public boolean createIndex(String indexName) {
        ESConnectionPool pool=null;
        TransportClient resource=null;
        Boolean success=false;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            if (indexExist(indexName)) {
                throw new RuntimeException("The index " + indexName + " already exits!");
            } else {
                CreateIndexResponse cIndexResponse = resource.admin().indices()
                        .create(new CreateIndexRequest(indexName))
                        .actionGet();
                if (cIndexResponse.isAcknowledged()) {
                    success=true;
                } else {
                    throw new RuntimeException("Fail to create index "+indexName+" !");
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
        return success;
    }

    /**
     * 新建索引
     * @param indexName 索引名
     * @param typeName 类型
     * @param mapping mapping
     * @return 是否创建成功
     */
    public boolean createIndex(String indexName, String typeName, XContentBuilder mapping) {
        ESConnectionPool pool=null;
        TransportClient resource=null;
        Boolean success=false;
        try
        {
            pool = getConnectionPool();
            resource = pool.getConnection();
            CreateIndexRequestBuilder crb = resource.admin().indices().prepareCreate(indexName);
            CreateIndexResponse createIndexResponse = crb.addMapping(typeName, mapping).execute().actionGet();
            if(createIndexResponse.isAcknowledged())
            {
                success = true;
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
        return success;
    }
    /**
     * 新建索引库
     * @param index 索引名
     * @param type 类型
     */
    public boolean createIndex(String index, String type) {
        ESConnectionPool pool=null;
        TransportClient resource=null;
        Boolean success=false;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            IndexResponse indexResponse = resource.prepareIndex(index, type).setSource().get();
            success=true;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource);
        }
        return success;
    }

//    public static void main(String[] args) {
//        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
//        queryBuilder.must(QueryBuilders.termQuery("vipType","2"));
//        queryBuilder.must(QueryBuilders.rangeQuery("regTime").lt("2018-09-09 13:00:00"));
//
//        ESClient.getInstance().delete_by_query_asyn("user",queryBuilder);
//    }
}

/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.util;

import org.apache.commons.lang3.StringUtils;

/**
 * 字符串操作工具类
 */
public class StrUtil {

    /**
     * 判断字符串数组是否等于指定的字符串
     * @param sourceStr 指定的字符串
     * @param equalsString 字符串数组
     * @return true包含，false不包含
     */
    public static Boolean equalsStringList(String sourceStr, String... equalsString)
    {
        if(StringUtils.isEmpty(sourceStr))
        {
            return false;
        }
        for (int i = 0; i < equalsString.length; i++) {
            boolean equals = equalsString[i].equals(sourceStr);
            if(equals)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断字符串数组是否包含指定的字符串开头
     * @param sourceStr 指定的字符串
     * @param prefixString 字符串数组
     * @return true包含，false不包含
     */
    public static Boolean prefixStringList(String sourceStr, String... prefixString)
    {
        if(StringUtils.isEmpty(sourceStr))
        {
            return false;
        }
        for (int i = 0; i < prefixString.length; i++) {
            boolean equals =sourceStr.startsWith(prefixString[i]);
            if(equals)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * 根据传进来的百分比，推断剩余还有多少百分比（可能不准），总共是100%
     * @param mainRatio 传进来的百分比，可以是多个
     */
    public static String reRatio(String... mainRatio)
    {
        Integer total = 100;
        for (int i = 0; i < mainRatio.length; i++) {
            Integer tmp = Integer.valueOf(mainRatio[i].replace("%",""));
            total = total - tmp;
        }
        return total + "%";
    }

    /**
     * 字符串反转
     * @param str 带反转的字符串
     * @return 反转后的结果
     */
    public static String reverse(String str) {
        if (str == null || str.equals(""))
            return str;
        byte[] bytes = str.getBytes();
        for (int l = 0, h = str.length() - 1; l < h; l++, h--) {
            byte temp = bytes[l];
            bytes[l] = bytes[h];
            bytes[h] = temp;
        }
        return new String(bytes);
    }

    /**
     * 移除最后一个字符
     */
    public static String removeLastChar(String str)
    {
        if(StringUtils.isEmpty(str))
            return "";
        str = str.substring(0,str.length()-1);
        return str;
    }

    /**将json字符串包装成ogg风格消息
     * @param table 表名
     * @param op_type 操作类型
     * @param primary_keys 主键
     * @param json 需要包装的json
     */
    public static String toOggJson(String table,String op_type,String[] primary_keys,String json)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"table\": \"");
        sb.append(table);
        sb.append("\",\"op_type\": \"");
        sb.append(op_type);
        sb.append("\",");
        sb.append("\"op_ts\": \"\",");
        sb.append("\"current_ts\": \"\",");
        sb.append("\"pos\": \"0\",");
        sb.append("\"primary_keys\": [");
        String[] clone = primary_keys.clone();
        for (int i = 0; i < clone.length; i++) {
            clone[i] = "\""+clone[i]+"\"";
        }
        String join = StringUtils.join(clone,",");
        sb.append(join);
        sb.append("],\"after\":");
        sb.append(json);
        sb.append("}");

        return sb.toString();
    }
}

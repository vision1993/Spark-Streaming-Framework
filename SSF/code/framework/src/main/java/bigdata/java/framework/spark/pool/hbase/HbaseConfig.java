/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.pool.hbase;

import bigdata.java.framework.spark.pool.ConnectionPoolConfig;
import com.typesafe.config.Config;

import java.util.Properties;

public class HbaseConfig extends ConnectionPoolConfig {

    Properties properties = new Properties();
    Properties poolProperties = new Properties();
    public HbaseConfig()
    {
        init();
    }
    public void init()
    {
        Config config = getConfig();
//        poolSize = config.getInt("hbasePool");
        properties = ConnectionPoolConfig.getProperties("hbaseparam.");
        poolProperties = ConnectionPoolConfig.getProperties("hbase.pool.");
        ConnectionPoolConfig.setPoolProperties(this,poolProperties);

    }

//    public Integer getPoolSize() {
//        return poolSize;
//    }
//
//    public void setPoolSize(Integer poolSize) {
//        this.poolSize = poolSize;
//    }
//
//    Integer poolSize;

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }
}

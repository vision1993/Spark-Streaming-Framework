

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for File
-- ----------------------------
DROP TABLE IF EXISTS `File`;
CREATE TABLE `File`  (
  `fileId` int(11) NOT NULL AUTO_INCREMENT,
  `fileName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '文件名',
  `dirType` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '目录类型',
  `filePath` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '存储路径',
  `fileSize` bigint(20) NULL DEFAULT NULL COMMENT '文件大小',
  `descText` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '描述',
  `hdfsPath` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'hdfs上的路径',
  `md5Code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '文件的md5值',
  `userId` int(11) NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`fileId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1422 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for KAFKAOFFSET
-- ----------------------------
DROP TABLE IF EXISTS `KAFKAOFFSET`;
CREATE TABLE `KAFKAOFFSET`  (
  `APPNAME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用程序名称',
  `TOPIC` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'kafka主题名称',
  `GROUPID` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消费者组名称',
  `PARTITION` int(255) NOT NULL COMMENT '分区名称',
  `LASTESTOFFSET` bigint(255) NOT NULL COMMENT '偏移量untilOffset',
  `UPTIME` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`APPNAME`, `TOPIC`, `GROUPID`, `PARTITION`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for STOPBYMARK
-- ----------------------------
DROP TABLE IF EXISTS `STOPBYMARK`;
CREATE TABLE `STOPBYMARK`  (
  `APPNAME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用程序名称',
  `FLAG` int(255) NOT NULL COMMENT '停止标记，1运行，0停止',
  `STOPTIME` bigint(255) NOT NULL COMMENT '多少秒后停止应用程序',
  `UPTIME` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`APPNAME`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for SparkJob
-- ----------------------------
DROP TABLE IF EXISTS `SparkJob`;
CREATE TABLE `SparkJob`  (
  `jobId` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `fileName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '文件名',
  `filePath` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '存储路径',
  `upTime` datetime(0) NULL DEFAULT NULL COMMENT '上传时间',
  `fileSize` bigint(20) NULL DEFAULT NULL COMMENT '文件大小',
  `descText` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '描述',
  `hdfsPath` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'hdfs上的路径',
  `isMian` int(11) NULL DEFAULT NULL COMMENT '当前使用的job',
  `md5Code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '文件的md5值',
  `userId` int(11) NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`jobId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 194 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for SysSet
-- ----------------------------
DROP TABLE IF EXISTS `SysSet`;
CREATE TABLE `SysSet`  (
  `conf` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `param` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `args` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `defaultJars` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT 'jars路径包含默认的jar包',
  `defaultFiles` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT '默认包含的files文件',
  `queueName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'yarn队列名称',
  `userId` int(11) NULL DEFAULT NULL COMMENT '用户id'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for Task
-- ----------------------------
DROP TABLE IF EXISTS `Task`;
CREATE TABLE `Task`  (
  `taskId` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `userId` int(11) NULL DEFAULT NULL,
  `taskType` int(11) NULL DEFAULT NULL COMMENT '1 sparkstreaming framework 2 flink 10 原生sparkstreaming ',
  `appName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `taskName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务名称',
  `classFullName` varchar(2000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '类全名称',
  `conf` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `args` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `jars` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `files` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `param` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `status` int(11) NULL DEFAULT NULL COMMENT '任务状态\r\n10停止\r\n11停止中\r\n20启动\r\n21启动中\r\n22Livy提交中',
  `queueName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'yarn队列名称',
  `jobId` int(11) NULL DEFAULT NULL,
  `errCount` int(255) NULL DEFAULT NULL COMMENT '错误次数',
  `tryCount` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '重启次数',
  `startTime` datetime(0) NULL DEFAULT NULL COMMENT '当天启动时间',
  `saveType` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'offset和优雅关闭保存位置MySql,Hbase,Redis',
  `activeBatch` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '积压批次数量短信告警',
  `restartActiveBatch` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '积压批次数量多少重启',
  `openRestartActiveBatch` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '是否开启积压批次数量自动重启程序',
  `webApi` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '是否开启webapi(true开启，false不开启)',
  `webApiToken` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'webApi调用秘钥',
  `openChart` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '开启chart图标',
  PRIMARY KEY (`taskId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 88 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for TaskChart
-- ----------------------------
DROP TABLE IF EXISTS `TaskChart`;
CREATE TABLE `TaskChart`  (
  `taskId` int(11) NOT NULL COMMENT '任务id',
  `topic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'topic',
  `consumeOffset` bigint(255) NULL DEFAULT NULL COMMENT '消费offset',
  `maxOffset` bigint(255) NULL DEFAULT NULL COMMENT 'topic最大offset',
  `uptime` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `p_time` datetime(0) NULL DEFAULT current_timestamp(),
  INDEX `index_uptime`(`uptime`) USING BTREE,
  INDEX `index_taskId`(`taskId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for TaskLog
-- ----------------------------
DROP TABLE IF EXISTS `TaskLog`;
CREATE TABLE `TaskLog`  (
  `taskLogId` int(11) NOT NULL AUTO_INCREMENT,
  `taskId` int(11) NULL DEFAULT NULL,
  `batchId` varchar(2000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'livy的id',
  `applicationid` varchar(2000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'yarn的applicationId',
  `session` varchar(2000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'livy的session',
  `driver` varchar(2000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'driver的id',
  `sparkUI` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'sparkUI的地址',
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '更新日期',
  `livyText` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT 'livy返回的json字符串',
  `waitingBatches` int(255) NULL DEFAULT NULL COMMENT '积压批次',
  `totalCompletedBatches` int(255) NULL DEFAULT NULL COMMENT '完成总数',
  `totalProcessedRecords` bigint(255) NULL DEFAULT NULL COMMENT '处理总记录数',
  `metrics` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT 'sparkUI返回的json统计数据',
  PRIMARY KEY (`taskLogId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 919 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for UserInfo
-- ----------------------------
DROP TABLE IF EXISTS `UserInfo`;
CREATE TABLE `UserInfo`  (
  `UserId` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `NickName` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '姓名',
  `UserName` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '用户名',
  `PassWord` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT '密码',
  `IsAdmin` int(255) NULL DEFAULT NULL COMMENT '是否管理员',
  `warnPhone` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '告警电话号码',
  PRIMARY KEY (`UserId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

INSERT INTO `UserInfo` VALUES (1, '稍稍', 'shaoshao', '123456', 1, '13666666666');

SET FOREIGN_KEY_CHECKS = 1;
